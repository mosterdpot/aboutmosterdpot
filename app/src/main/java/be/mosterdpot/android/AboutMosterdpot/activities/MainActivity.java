package be.mosterdpot.android.AboutMosterdpot.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vansuita.materialabout.sample.R;
import be.mosterdpot.android.AboutMosterdpot.helper.SampleHelper;

/**
 *
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sample_view);
        SampleHelper.with(this).init().loadAbout();
    }
}
