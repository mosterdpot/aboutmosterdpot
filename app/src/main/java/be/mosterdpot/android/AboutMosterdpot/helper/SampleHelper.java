package be.mosterdpot.android.AboutMosterdpot.helper;

import android.app.Activity;
import android.content.Intent;
import android.widget.FrameLayout;

import com.vansuita.materialabout.builder.AboutBuilder;
import com.vansuita.materialabout.sample.R;
import com.vansuita.materialabout.views.AboutView;

/**
 * Created by jrvansuita on 17/02/17.
 */
public class SampleHelper {

    private Activity activity;

    private SampleHelper(Activity activity) {
        this.activity = activity;
    }

    public static SampleHelper with(Activity activity) {
        return new SampleHelper(activity);
    }

    public SampleHelper init() {
        int theme = R.style.AppThemeLight;
        activity.setTheme(theme);

        return this;
    }

    public void loadAbout() {
        final FrameLayout flHolder = activity.findViewById(R.id.about);

        AboutBuilder builder = AboutBuilder.with(activity)
                .setAppName(R.string.app_name)
                .setPhoto(R.drawable.ic_mosterdpot_round)
                .setAppIcon(R.mipmap.ic_launcher)
                .setCover(R.mipmap.profile_cover)
                .setLinksAnimated(true)
                .setDividerDashGap(13)
                .setName("Mosterdpot")
                .setSubTitle("A curios development team")
                .setLinksColumnsCount(4)
                .setBrief("I'm warmed of mobile technologies. Ideas maker, curious and nature lover.")
                .addGooglePlayStoreLink("8049023019809067219")
                .addGitHubLink("EenMosterdpot")
//                .addBitbucketLink("jrvansuita")
                .addFacebookLink("eenMosterdpot")
//                .addTwitterLink("user")
//                .addInstagramLink("jnrvans")
                .addGooglePlusLink("106301512184148347896")
//                .addYoutubeChannelLink("CaseyNeistat")
//                .addDribbbleLink("user")
//                .addLinkedInLink("arleu-cezar-vansuita-júnior-83769271")
                .addEmailLink("info@mosterdpot.be")
//                .addWhatsappLink("Jr", "+554799650629")
//                .addSkypeLink("user")
                .addGoogleLink("106301512184148347896")
//                .addAndroidLink("user")
                .addWebsiteLink("http://mosterdpot.be")
//                .addFiveStarsAction()
//                .addMoreFromMeAction("Vansuita")
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .addUpdateAction()
                .setActionsColumnsCount(2)
                .addFeedbackAction("info@mosterdpot.be")
//                .addPrivacyPolicyAction("http://www.docracy.com/2d0kis6uc2")
                .addIntroduceAction((Intent) null)
                .addHelpAction((Intent) null)
                .addChangeLogAction((Intent) null)
                .addRemoveAdsAction((Intent) null)
                .addDonateAction((Intent) null)
                .setWrapScrollView(true)
                .setShowAsCard(true);

        AboutView view = builder.build();

        flHolder.addView(view);
    }
}